# Infrastructure Daemon

Infrastructure Daemon (InfraD for short) manages deployments of "scenarios" to virtualization platforms.

Scenarios represent a combination of one or more virtual systems and networks which can be configurable via input
variables. A "system" is typically a virtual machine, LXC container, or Docker container (deployed to kubernetes).

## Development

All dependencies can be installed using `poetry install`.

A development instance of the project can be run with `poetry run dev`.

An instance of [Hasura](https://gitlab.com/c2-games/scoring/hasura) must also be running. By default,
hasura on `http://localhost:8080` is used, but this can be configured using the `TODO VAR` environment variable.
