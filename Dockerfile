FROM python:3.10
LABEL MAINTAINER="C2 Games <support@c2games.org>"

WORKDIR /app
COPY ./poetry.lock ./pyproject.toml ./
ENV PYTHONPATH=/src
ENV PYTHONUNBUFFERED=1

RUN pip install "poetry==1.2.1"
RUN poetry config virtualenvs.create false && \
    poetry install --only main

COPY infrastructure_daemon /src/app

ENTRYPOINT ["poetry", "run"]
CMD ["prod"]
