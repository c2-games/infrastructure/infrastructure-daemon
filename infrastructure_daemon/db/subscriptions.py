from typing import AsyncIterator

from gql import Client

from infrastructure_daemon.models.deployments import Deployment
from infrastructure_daemon.models.jobs import Job


async def watch_for_pending_jobs(client: Client) -> AsyncIterator[Job]:
    """
    Watch for jobs with the pending status.
    """
    async for job in Job.subscribe_async(
        client,
        # look up pending jobs
        where='{ status: {_eq: "pending" }}',
    ):
        if not isinstance(job, Job):
            continue  # typing

        print(job)
        yield job


async def watch_for_pending_deployments(client: Client) -> AsyncIterator[Deployment]:
    """
    Watch for deployments with in any pending state
    """
    # todo switch away from stream
    async for deployment in Deployment.subscribe_async(
        client,
        # find any deployments with a pending state
        where={"state": {"_like": "pending_%"}},
    ):
        if not isinstance(deployment, Deployment):
            continue  # typing

        print("got new deployment!", deployment)
        yield deployment
