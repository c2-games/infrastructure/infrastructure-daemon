from typing import TYPE_CHECKING, Type

from gql import Client
from gql.dsl import DSLSchema, DSLType
from gql.transport.aiohttp import AIOHTTPTransport
from gql.transport.websockets import WebsocketsTransport
from graphql import GraphQLSchema
from pydantic import BaseModel

if TYPE_CHECKING:
    from infrastructure_daemon.models import GqlBaseModel


class SchemaCache(BaseModel, arbitrary_types_allowed=True):
    """
    This class exists to cache the schema once it's received from the remote server with the option
    `fetch_schema_from_transport` during the client initialization in `get_client`.

    A simple variable cannot be used because global python variables cannot be set within a function, but attributes of
    global variables can be. This could just be a dictionary, but a pydantic model provides better typing and the
    ability to add helper functions for retrieval of objects from the schema.
    """

    # these must be optional because we must initialize this class before connection to the server, but they should
    # never be None after the initial connection has been made.
    gql: GraphQLSchema | None = None
    dsl: DSLSchema | None = None

    @property
    def query(self) -> DSLType:
        """Get Query Root"""
        if not (self.gql and self.gql.query_type):
            raise RuntimeError("Schema not initialized")
        return getattr(self.dsl, self.gql.query_type.name)

    @property
    def subscription(self) -> DSLType:
        """Get Subscription Root"""
        if not (self.gql and self.gql.subscription_type):
            raise RuntimeError("Schema not initialized")
        return getattr(self.dsl, self.gql.subscription_type.name)

    @property
    def mutation(self) -> DSLType:
        """Get Mutation Root"""
        if not (self.gql and self.gql.mutation_type):
            raise RuntimeError("Schema not initialized")
        return getattr(self.dsl, self.gql.mutation_type.name)

    def get_model_schema(self, model: Type["GqlBaseModel"]) -> DSLType:
        """Get GQL Schema for a model"""
        if not (self.gql and self.gql.query_type):
            raise RuntimeError("Schema not initialized")
        return getattr(self.dsl, model.GqlConfig.schema_table_name)


# create an instance of the schema cache, as setting the global variable directly from a function is not allowed
schema: SchemaCache = SchemaCache()


async def get_client(transport_cls: Type[AIOHTTPTransport | WebsocketsTransport] = AIOHTTPTransport) -> Client:
    # todo handle permissions
    headers = {"x-hasura-admin-secret": "myadminsecretkey"}
    transport: AIOHTTPTransport | WebsocketsTransport

    # todo make connection url configurable
    if transport_cls is WebsocketsTransport:
        transport = WebsocketsTransport(
            url="ws://localhost:8080/v1/graphql",
            headers=headers,
            # increase max size of payloads
            connect_args={"max_size": 9999999},
        )
    else:
        transport = AIOHTTPTransport(url="http://localhost:8080/v1/graphql", headers=headers)

    if schema.gql:
        # return early with known schema
        return Client(transport=transport, schema=schema.gql)

    # fetch and cache schema
    client = Client(transport=transport, fetch_schema_from_transport=True)
    await client.connect_async()

    if not client.schema:
        raise RuntimeError("Could not fetch schema from server")

    schema.gql = client.schema
    schema.dsl = DSLSchema(client.schema)

    # Close transport so it can be re-opened later
    await transport.close()
    return client
