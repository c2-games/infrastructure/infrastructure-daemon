from gql import Client
from gql.transport.aiohttp import AIOHTTPTransport

from infrastructure_daemon.db import get_client
from infrastructure_daemon.models.deployments import Deployment
from infrastructure_daemon.models.jobs import Job


async def handle_job(job: Job, client: Client | None = None) -> None:
    print("starting job")
    # use HTTP for simple queries to save websocket connections on remote server
    async with client or await get_client(transport_cls=AIOHTTPTransport) as session:
        try:
            job.status = "complete"
            await Job.upsert(session, job)
            print("job completed successfully")
        except Exception as e:
            job.status = "failed"
            await Job.upsert(session, job)
            print(e)  # todo err print


async def handle_deployment(deployment: Deployment) -> None:
    try:
        # use HTTP for simple queries to save websocket connections on remote server
        async with await get_client(transport_cls=AIOHTTPTransport) as session:
            print("acquiring deployment")
            await deployment.acquire(session)

            print("deployment acquired, inserting new job")
            idx, result = await Job.upsert(session, deployment.to_job(), dump_args={"exclude": ["id"]})

            if not (result.data and result.data.get(idx)):
                raise RuntimeError("failed to insert job")

            await handle_job(Job(**result.data["insert_infrastructure_jobs_one"]))
    except Exception as e:
        print(e)  # todo error print
