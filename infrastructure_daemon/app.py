import argparse
import asyncio

import uvicorn
from gql import Client
from gql.transport.websockets import WebsocketsTransport

from infrastructure_daemon.db import get_client
from infrastructure_daemon.db.subscriptions import watch_for_pending_deployments
from infrastructure_daemon.handlers import handle_deployment


async def deployment_loop(client: Client) -> None:
    async for deployment in watch_for_pending_deployments(client):
        await handle_deployment(deployment)


async def main(loop: asyncio.AbstractEventLoop) -> None:
    # watch for new deployments in the background
    loop.create_task(deployment_loop(await get_client(transport_cls=WebsocketsTransport)))


def run() -> asyncio.AbstractEventLoop:
    loop = asyncio.get_event_loop()
    loop.create_task(main(loop))
    return loop


def prod() -> None:
    """Run production server"""
    run().run_forever()


def dev(reload: bool = True) -> None:
    """Run development server with live reloading"""
    # Using uvicorn here is a bit of a hack to enable hot-reloading of the async app.
    # We specify port zero so that uvicorn selects a random port to "listen" on and avoid conflicts,
    # even though nothing is actually listening there.
    # Accessing the port via HTTP will cause an internal server error.
    uvicorn.run("infrastructure_daemon.app:run", reload=reload, factory=True, port=0)


if __name__ == "__main__":
    # parse command line arguments for dev or prod server
    parser = argparse.ArgumentParser()
    parser.add_argument("--dev", action="store_true", help="Run development server")
    args = parser.parse_args()
    if args.dev:
        dev()
    else:
        prod()
