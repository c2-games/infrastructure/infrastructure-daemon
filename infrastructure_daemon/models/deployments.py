from datetime import datetime
from typing import ClassVar
from uuid import UUID

from gql.client import AsyncClientSession, SyncClientSession

from infrastructure_daemon.models import GqlBaseModel, GqlConfigCls
from infrastructure_daemon.models.jobs import Job


class Deployment(GqlBaseModel):
    GqlConfig: ClassVar[GqlConfigCls] = GqlConfigCls(
        schema="infrastructure",
        table="deployments",
        primary_key="id",
        update_cols=["state", "updated"],
        update_constraint="deployments_pkey",
    )

    id: UUID
    user_id: UUID
    scenario_id: UUID
    state: str  # todo enum
    created: datetime
    updated: datetime

    async def acquire(self, session: SyncClientSession | AsyncClientSession) -> None:
        """
        Acquire a pending state for execution. This should be called before performing any actions on the deployment.
        """
        new_state = "creating"  # todo update
        root_field, query = self.update_query(
            where={
                "_and": [
                    {"id": {"_eq": str(self.id)}},
                    {"state": {"_eq": self.state}},
                ]
            },
            set={"state": new_state, "updated": datetime.utcnow().isoformat()},
            operation_name="AcquireDeployment",
        )

        result = await self.execute(session, query)
        data = result.data.get(root_field) if result.data else None

        if not (data and data["affected_rows"]):
            raise ValueError("failed to update state; job already acquired?")

        self.state = data["returning"][0]["state"]
        self.updated = data["returning"][0]["updated"]

    def to_job(self) -> Job:
        return Job(
            status="pending",
            deployment_id=self.id,
        )
