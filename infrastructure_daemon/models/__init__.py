from typing import Any, AsyncIterator, ClassVar, Dict, Generic, Iterator, List, Literal, Optional, Tuple, TypeVar

from gql import Client
from gql.client import AsyncClientSession, SyncClientSession
from gql.dsl import DSLField, DSLMutation, DSLQuery, DSLSubscription, DSLVariable, DSLVariableDefinitions, dsl_gql
from gql.transport import AsyncTransport
from graphql import DocumentNode, ExecutionResult, print_ast
from pydantic import BaseModel, Field

from infrastructure_daemon.db import schema

GqlExecutor = SyncClientSession | AsyncClientSession | Client
# Returns the key used to index into the Result and the query itself
GeneratedQueryReturn = Tuple[str, DocumentNode]
OrderBy = List[
    Dict[str, Literal["asc", "desc", "asc_nulls_first", "asc_nulls_last", "desc_nulls_first", "desc_nulls_last"]]
]


# -----------------------------HERE BE DRAGONS-----------------------------------
#                                        \/
#                                        ^`'.
#                                        ^   `'.
#              (                         ^      `'.
#            )  )        \/              ^         `'.
#          (   ) @       /^              ^            `'.
#        )  )) @@  )    /  ^             ^               `'.
#       ( ( ) )@@      /    ^            ^                  `'.
#     ))  ( @@ @ )    /      ^           ^                     `'.
#    ( ( @@@@@(@     /       |\_/|,      ^                        `'.
#   )  )@@@(@@@     /      _/~/~/~|C     ^                           `'.
# ((@@@(@@@@@(     /     _(@)~(@)~/\C    ^                              `'.
#   ))@@@(@@)@@   /     /~/~/~/~/`\~`C   ^             __.__               `'.
#    )@@@@(@@)@@@(     (o~/~o)^,) \~ \C  ^          .' -_'-'"...             `.
#     ( (@@@)@@@(@@@@@@_~^~^~,-/\~ \~ \C/^        /`-~^,-~-`_~-^`;_           `.
#       @ )@@@(@@@@@@@   \^^^/  (`^\.~^ C^.,  /~^~^~^/_^-_`~-`~-~^\- /`'-./`'-. ;
#        (@ (@@@@(@@      `''  (( ~  .` .,~^~^-`-^~`/'^`-~ _`~-`_^-~\         ^^
#            @jgs@             (((` ~ .-~-\ ~`-_~`-/_-`~ `- ~-_- `~`;
#           /                 /~((((` . ~-~\` `  ~ |:`-_-~_~`  ~ _`-`;
#          /   Art by        /~-((((((`.\-~-\ ~`-`~^\- ^_-~ ~` -_~-_`~`;
#         /     joan stark  /-~-/(((((((`\~-~\~`^-`~`\ -~`~\-^ -_~-_`~-`;
#        /                 /~-~/  `((((((|-~-|((`.-~.`Y`_,~`\ `,- ~-_`~-`;
#       /              ___/-~-/     `""""|~-~|"''    /~-^ .'    `:~`-_`~-~`;
#      /         _____/  /~-~/           |-~-|      /-~-~.`      `:~^`-_`^-:
#     /    _____/        ((((            ((((      (((((`           `:~^-_~-`;
#     \___/                                                          `:_^-~`;
#                                                                     `:~-^`:
#                                                                   ,`~-~`,`
#                                                                  ,"`~.,'
#                                                                ,"-`,"`
#                                                              ,"_`,"
#                                                             ,","`
#                                                          ;~-~_~~;
#                                                           '. ~.'
# -----------------------------HERE BE DRAGONS-----------------------------------
class FailedExecution(Exception):
    result: ExecutionResult
    query: DocumentNode

    def __init__(self, result: ExecutionResult, query: DocumentNode):
        self.result = result
        self.query = query


class GqlConfigCls(BaseModel):
    #: DB Schema Prefix
    table_schema: Optional[str] = Field(alias="schema")
    #: DB Table name
    table: str
    #: Primary Key
    primary_key: str
    update_constraint: str
    update_cols: List[str]  # todo add validator with default values

    # @field_validator("update_constraint", pre=True)
    # # todo this isn't working
    # def _update_constraint_default(cls, v, values):
    #     return v or f"{values['table']}_pkey"

    @property
    def schema_table_name(self) -> str:
        schema_name = f"{self.table_schema}_" if self.table_schema else ""
        return f"{schema_name}{self.table}"


class _GqlBaseModel(BaseModel):
    GqlConfig: ClassVar[GqlConfigCls]


# Generic type to utilize for type hinting. Separate _GqlBaseModel and GqlBaseModel are used so that methods from
# GqlBaseModel can return their own type. Basically, this lets us say:
# RealModel.subscribe() returns an instance of RealModel
GqlBaseModelT = TypeVar("GqlBaseModelT", bound="_GqlBaseModel")


class GqlBaseModel(BaseModel, Generic[GqlBaseModelT]):
    GqlConfig: ClassVar[GqlConfigCls]

    @classmethod
    def subscription_query(
        cls, operation_name: str = None, fields: List[str] = None, where: Dict[str, Any] = None
    ) -> GeneratedQueryReturn:
        # see upsert_query() for details comments on the GQL lib
        root_field = cls.GqlConfig.schema_table_name
        query = getattr(schema.subscription, root_field)
        model_schema = schema.get_model_schema(cls)

        for field in fields or cls.model_fields.keys():
            query.select(getattr(model_schema, field))

        if where:
            query(where=where)

        op = DSLSubscription(query)
        return root_field, dsl_gql(**{operation_name or f"infrad_subscribe_{cls.GqlConfig.schema_table_name}": op})

    @classmethod
    def subscribe(cls, client: Client, **kwargs: Any) -> Iterator["GqlBaseModel"]:
        idx, query = cls.subscription_query(**kwargs)
        print(print_ast(query))  # todo debug print
        for result in client.subscribe(query):
            for item in result[idx]:
                yield cls(**item)

    @classmethod
    async def subscribe_async(cls, client: Client, **kwargs: Any) -> AsyncIterator["GqlBaseModel"]:
        idx, query = cls.subscription_query(**kwargs)
        print(print_ast(query))  # todo debug print
        async for result in client.subscribe_async(query):
            for item in result[idx]:
                yield cls(**item)

    @classmethod
    def streaming_subscription_query(
        cls,
        initial_value: Dict[str, Any],
        order: Literal["ASC", "DESC"] = "ASC",
        batch_size: int = 1,
        operation_name: str = None,
        fields: None | List[str] = None,
        where: Optional[Dict[str, Any]] = None,
    ) -> GeneratedQueryReturn:
        """
        Create a streaming subscription query. Streams are special to hasura and will send data in BATCH_SIZE chunks
        rather than sending all data repeatedly like a standard subscription.

        https://hasura.io/docs/latest/subscriptions/postgres/streaming/index/

        :param initial_value: Dictionary defining the cursor's initial value. Ex, {created: "2023-01-01"}
        :param order: Order of results, either ASC (ascending) or DSC (descending)
        :param batch_size: Number of results to return at one time
        :param operation_name: Name of query. Will be generated based on table name if not provided.
        :param fields: List of fields to return. Defaults to all fields defined on the model.
        :param where: Where clause to filter results. Ex, {status: {_eq: "pending"}}
        :return: GQL DocumentNode containing a streaming subscription query. Use print_ast to view as a string.
        """
        # see upsert_query() for details comments on the GQL lib
        root_field = f"{cls.GqlConfig.schema_table_name}_stream"
        query = getattr(schema.subscription, root_field)
        model_schema = schema.get_model_schema(cls)

        for field in fields or cls.model_fields.keys():
            query.select(getattr(model_schema, field))

        query(batch_size=batch_size, cursor=[{"initial_value": initial_value, "ordering": order}])
        if where:
            query(where=where)

        op = DSLSubscription(query)
        return root_field, dsl_gql(**{operation_name or f"infrad_stream_{cls.GqlConfig.schema_table_name}": op})

    @classmethod
    def stream(cls, client: Client, **kwargs: Any) -> Iterator["GqlBaseModel"]:
        # utilize special hasura streaming subscriptions
        # https://hasura.io/docs/latest/subscriptions/postgres/streaming/use-cases/
        idx, query = cls.streaming_subscription_query(**kwargs)
        print(print_ast(query))  # todo debug print
        for result in client.subscribe(query):
            for item in result[idx]:
                yield cls(**item)

    @classmethod
    async def stream_async(cls, client: Client, **kwargs: Any) -> AsyncIterator["GqlBaseModel"]:
        idx, query = cls.streaming_subscription_query(**kwargs)
        print(print_ast(query))  # todo debug print
        async for result in client.subscribe_async(query):
            for item in result[idx]:
                yield cls(**item)

    @classmethod
    def _add_returning_fields(cls, fields: List[str] | None, query: DSLField) -> None:
        """
        Add model fields to a query for the `returning` statement. This is used by insert_* and update_* queries.
        :param fields: List of fields to return. Specifying None will add all known fields. Specifying an empty array
                       will not include returning{} at all. `affected_rows` will always be included.
        :param query: DSLField to add fields onto.
        :return: None - query is modified in place.
        """
        # get response_schema, which contains `affected_rows` and `returning`
        response_schema = getattr(schema.dsl, f"{cls.GqlConfig.schema_table_name}_mutation_response")
        # always select affected rows
        query.select(response_schema.affected_rows)

        # if fields is an empty array, an error would occur from adding a `returning` statement with no fields
        # if fields is None, we'll add all fields below
        if fields == []:
            return

        # get returning_schema, which determines the fields from the model that are returned
        returning_schema = response_schema.returning
        # use the returning_schema in the main query
        query.select(returning_schema)
        # get model schema for selecting fields
        model_schema = schema.get_model_schema(cls)

        # returned model fields are selected straight from the model_schema and added to the returning_schema
        for field in fields or cls.model_fields.keys():
            returning_schema.select(getattr(model_schema, field))

    @classmethod
    def upsert_query(cls, many: bool, fields: List[str] = None, operation_name: str = None) -> GeneratedQueryReturn:
        # initialize the variable definitions
        var = DSLVariableDefinitions()
        # we define the arguments to query(...) as a dict because we need to define either
        # `object` or `objects` as a keyword arg, depending on the value of `many`.
        args: Dict[str, Dict | DSLVariable] = {
            "on_conflict": {"constraint": cls.GqlConfig.update_constraint, "update_columns": cls.GqlConfig.update_cols}
        }

        if many:
            # Friendly name for the query. Ex, mutation MyQuery() {...}
            operation_name = operation_name or f"upsert_{cls.GqlConfig.schema_table_name}"
            # actual operation to perform. Ex, mutation { insert_infrastructure_jobs_one() {} }
            root_field = f"insert_{cls.GqlConfig.schema_table_name}"
            # create input variable in query and in VariableDefinition class (var definition is used later)
            args["objects"] = var.variables["input"] = DSLVariable("input")
        else:
            operation_name = operation_name or f"upsert_{cls.GqlConfig.schema_table_name}_one"
            root_field = f"insert_{cls.GqlConfig.schema_table_name}_one"
            # create input variable in query and in VariableDefinition class (var definition is used later)
            args["object"] = var.variables["input"] = DSLVariable("input")

        # Create the query as a mutation
        query = getattr(schema.mutation, root_field)
        # add arguments to query (input vars and on_conflict behavior). Ex, the $input in:
        #   insert_infrastructure_jobs_one(
        #     on_conflict: {constraint: jobs_pkey, update_columns: [status, details]}
        #     object: $input
        #   )
        query(**args)

        # select return fields (defaults to all fields defined on the model)
        if many:
            cls._add_returning_fields(fields, query)
        else:
            model_schema = schema.get_model_schema(cls)
            for field in fields or cls.model_fields.keys():
                query.select(getattr(model_schema, field))

        # create mutation operation and add variable definitions.
        # variable definitions here is the top level input variable - ex, the $input in:
        # mutation upsert_infrastructure_jobs_one($input: infrastructure_jobs_insert_input!)
        op = DSLMutation(query)
        op.variable_definitions = var

        # create gql with query name defined
        return root_field, dsl_gql(**{operation_name: op})

    @classmethod
    async def upsert(
        cls,
        executor: GqlExecutor,
        model: "GqlBaseModel" | List["GqlBaseModel"],
        dump_args: Dict[str, Any] = None,
        **kwargs: Any,
    ) -> Tuple[str, ExecutionResult]:
        if isinstance(model, list):
            variables = [m.model_dump(mode="json", **dump_args or {}) for m in model]
            idx, query = cls.upsert_query(**kwargs, many=True)
        else:
            variables = model.model_dump(mode="json", **dump_args or {})  # type: ignore[assignment]
            idx, query = cls.upsert_query(**kwargs, many=False)

        return idx, await cls.execute(executor, query, variable_values={"input": variables})

    @classmethod
    def update_query(
        cls,
        where: Dict[str, Any],
        set: Dict[str, Any],  # pylint: disable=redefined-builtin
        fields: List[str] = None,
        operation_name: str = None,
    ) -> GeneratedQueryReturn:
        # see upsert_query() for details comments on the GQL lib
        root_field = f"update_{cls.GqlConfig.schema_table_name}"
        query = getattr(schema.mutation, root_field)
        query(where=where, _set=set)
        cls._add_returning_fields(fields, query)
        op = DSLMutation(query)
        return root_field, dsl_gql(**{operation_name or f"infrad_update_{cls.GqlConfig.schema_table_name}": op})

    @classmethod
    def fetch_query(
        cls,
        where: Dict[str, Any] = None,
        fields: List[str] = None,
        operation_name: str = None,
        order_by: OrderBy = None,
        offset: int = None,
        limit: int = None,
        distinct_on: str = None,
    ) -> GeneratedQueryReturn:
        # see upsert_query() for details comments on the GQL lib
        root_field = cls.GqlConfig.schema_table_name
        query = getattr(schema.query, root_field)
        if where:
            query(where=where)
        if order_by:
            query(order_by=order_by)
        if offset:
            query(offset=offset)
        if limit:
            query(limit=limit)
        if distinct_on:
            query(distinct_on=distinct_on)

        model_schema = schema.get_model_schema(cls)
        for field in fields or cls.model_fields.keys():
            query.select(getattr(model_schema, field))

        op = DSLQuery(query)
        return root_field, dsl_gql(**{operation_name or f"infrad_query_{cls.GqlConfig.schema_table_name}": op})

    @classmethod
    async def execute(cls, executor: GqlExecutor, query: DocumentNode, **kwargs: Any) -> ExecutionResult:
        print(print_ast(query), kwargs)
        if isinstance(executor, SyncClientSession):
            result = executor.execute(query, **kwargs, get_execution_result=True)
        elif isinstance(executor, AsyncClientSession):
            result = await executor.execute(query, **kwargs, get_execution_result=True)
        elif isinstance(executor.transport, AsyncTransport):
            result = await executor.execute_async(query, **kwargs)
        else:
            result = executor.execute(query, **kwargs)

        if result.errors:
            raise FailedExecution(result=result, query=query)

        return result
