from typing import Any, ClassVar, Dict, Optional
from uuid import UUID

from pydantic import Field

from infrastructure_daemon.models import GqlBaseModel, GqlConfigCls


class Job(GqlBaseModel):
    GqlConfig: ClassVar[GqlConfigCls] = GqlConfigCls(
        schema="infrastructure",
        table="jobs",
        primary_key="id",
        update_cols=["status", "details"],
        update_constraint="jobs_pkey",
    )

    id: Optional[UUID] = None
    # todo don't hardcode this testing value
    requester: UUID = UUID("00000000-0000-0000-0000-000000000000")
    deployment_id: UUID
    status: str  # todo enum
    # todo define more information about the details
    # todo do we include state in the details? do we add a field for it?
    details: Dict[str, Any] = Field(default_factory=dict)
    plugin: str | None = None
    # todo these aren't in hasura yet
    # logs: Optional[str]
    # execution_plugin_version: Optional[str]
    # worker_id: Optional[str]
