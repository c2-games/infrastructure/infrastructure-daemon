import json
from pathlib import Path
from typing import cast

from gql.dsl import DSLSchema
from graphql import build_client_schema, IntrospectionQuery
from infrastructure_daemon.db import schema


def load_test_schema():
    if schema.gql:
        return  # don't reload if not needed

    with open(Path(__file__, "..", "schema.json").resolve(), "r") as f:
        schema_raw = cast(IntrospectionQuery, json.loads(f.read()))

    schema.gql = build_client_schema(schema_raw)
    schema.dsl = DSLSchema(schema.gql)
