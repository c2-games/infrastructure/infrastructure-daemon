import unittest

from infrastructure_daemon.db import schema


class TestSchemaCache(unittest.TestCase):
    def test_query_property_raises_without_schema(self):
        self.assertIsNone(schema.gql)
        with self.assertRaises(RuntimeError):
            _ = schema.query

    def test_subscription_property_raises_without_schema(self):
        self.assertIsNone(schema.gql)
        with self.assertRaises(RuntimeError):
            _ = schema.subscription

    def test_mutation_property_raises_without_schema(self):
        self.assertIsNone(schema.gql)
        with self.assertRaises(RuntimeError):
            _ = schema.mutation

    def test_model_schema_property_raises_without_schema(self):
        self.assertIsNone(schema.gql)
        with self.assertRaises(RuntimeError):
            _ = schema.get_model_schema(None)


if __name__ == "__main__":
    unittest.main()
