from datetime import datetime
from typing import ClassVar
from uuid import UUID

from gql import gql
from graphql import print_ast, DocumentNode

from infrastructure_daemon.models import GqlBaseModel, GqlConfigCls

import unittest

from tests.utils import load_test_schema


class SampleModel(GqlBaseModel):
    GqlConfig: ClassVar[GqlConfigCls] = GqlConfigCls(
        schema="infrastructure",
        table="deployments",
        primary_key="id",
        update_cols=["state", "updated"],
        update_constraint="deployments_pkey",
    )

    id: UUID
    user_id: UUID
    scenario_id: UUID
    state: str
    created: datetime
    updated: datetime


class QueryTestCase(unittest.TestCase):
    def setUp(self) -> None:
        load_test_schema()

    def assertAstEqual(self, a: DocumentNode, b: DocumentNode):
        self.assertEquals(print_ast(a), print_ast(b))


class TestGqlBaseModelUpsertQueries(QueryTestCase):
    def test_upsert_query__one(self):
        root_field, query = SampleModel.upsert_query(many=False)
        self.assertEquals(root_field, "insert_infrastructure_deployments_one")
        expected = gql(
            """
            mutation upsert_infrastructure_deployments_one($input: infrastructure_deployments_insert_input!) {
              insert_infrastructure_deployments_one(
                on_conflict: {
                  constraint: deployments_pkey,
                  update_columns: [state, updated]
                }
                object: $input
              ) {
                id
                user_id
                scenario_id
                state
                created
                updated
              }
            }"""
        )
        self.assertAstEqual(query, expected)

    def test_upsert_query__one__query_name(self):
        root_field, query = SampleModel.upsert_query(many=False, operation_name="Asdf")
        self.assertEquals(root_field, "insert_infrastructure_deployments_one")
        expected = gql(
            """
            mutation Asdf($input: infrastructure_deployments_insert_input!) {
              insert_infrastructure_deployments_one(
                on_conflict: {
                  constraint: deployments_pkey,
                  update_columns: [state, updated]
                }
                object: $input
              ) {
                id
                user_id
                scenario_id
                state
                created
                updated
              }
            }"""
        )
        self.assertAstEqual(query, expected)

    def test_upsert_query__one__fields(self):
        root_field, query = SampleModel.upsert_query(many=False, fields=["id", "state"])
        self.assertEquals(root_field, "insert_infrastructure_deployments_one")
        expected = gql(
            """
            mutation upsert_infrastructure_deployments_one($input: infrastructure_deployments_insert_input!) {
              insert_infrastructure_deployments_one(
                on_conflict: {
                  constraint: deployments_pkey,
                  update_columns: [state, updated]
                }
                object: $input
              ) {
                id
                state
              }
            }"""
        )
        self.assertAstEqual(query, expected)

    def test_upsert_query__many(self):
        root_field, query = SampleModel.upsert_query(many=True)
        self.assertEquals(root_field, "insert_infrastructure_deployments")
        expected = gql(
            """
            mutation upsert_infrastructure_deployments($input: [infrastructure_deployments_insert_input!]!) {
              insert_infrastructure_deployments(
                on_conflict: {constraint: deployments_pkey, update_columns: [state, updated]}
                objects: $input
              ) {
                affected_rows
                returning {
                  id
                  user_id
                  scenario_id
                  state
                  created
                  updated
                }
              }
            }"""
        )
        self.assertAstEqual(query, expected)

    def test_upsert_query__many__query_name(self):
        root_field, query = SampleModel.upsert_query(many=True, operation_name="Asdf")
        self.assertEquals(root_field, "insert_infrastructure_deployments")
        expected = gql(
            """
            mutation Asdf($input: [infrastructure_deployments_insert_input!]!) {
              insert_infrastructure_deployments(
                on_conflict: {constraint: deployments_pkey, update_columns: [state, updated]}
                objects: $input
              ) {
                affected_rows
                returning {
                  id
                  user_id
                  scenario_id
                  state
                  created
                  updated
                }
              }
            }"""
        )
        self.assertAstEqual(query, expected)

    def test_upsert_query__many__fields(self):
        root_field, query = SampleModel.upsert_query(many=True, fields=["id", "state"])
        self.assertEquals(root_field, "insert_infrastructure_deployments")
        expected = gql(
            """
            mutation upsert_infrastructure_deployments($input: [infrastructure_deployments_insert_input!]!) {
              insert_infrastructure_deployments(
                on_conflict: {constraint: deployments_pkey, update_columns: [state, updated]}
                objects: $input
              ) {
                affected_rows
                returning {
                  id
                  state
                }
              }
            }"""
        )
        self.assertAstEqual(query, expected)

    def test_upsert_query__many__no_fields(self):
        root_field, query = SampleModel.upsert_query(many=True, fields=[])
        self.assertEquals(root_field, "insert_infrastructure_deployments")
        expected = gql(
            """
            mutation upsert_infrastructure_deployments($input: [infrastructure_deployments_insert_input!]!) {
              insert_infrastructure_deployments(
                on_conflict: {constraint: deployments_pkey, update_columns: [state, updated]}
                objects: $input
              ) {
                affected_rows
              }
            }"""
        )
        self.assertAstEqual(query, expected)


class TestGqlBaseModelUpdateQueries(QueryTestCase):
    def test_update_query(self):
        root_field, query = SampleModel.update_query(where={"id": {"_eq": "1234"}}, set={"state": "asdf"})
        self.assertEquals(root_field, "update_infrastructure_deployments")
        expected = gql(
            """
            mutation infrad_update_infrastructure_deployments {
              update_infrastructure_deployments(
                where: {id: {_eq: "1234"}}
                _set: {state: "asdf"}
              ) {
                affected_rows
                returning {
                  id
                  user_id
                  scenario_id
                  state
                  created
                  updated
                }
              }
            }"""
        )
        self.assertAstEqual(query, expected)

    def test_update_query__where_complex(self):
        root_field, query = SampleModel.update_query(
            set={"state": "asdf"},
            where={
                "_and": [
                    {"id": {"_eq": "asdf"}},
                    {"state": {"_eq": "asdf"}},
                ]
            },
        )
        self.assertEquals(root_field, "update_infrastructure_deployments")
        expected = gql(
            """
            mutation infrad_update_infrastructure_deployments {
              update_infrastructure_deployments(
                where: {
                  _and: [
                    {id: {_eq: "asdf"}},
                    {state: {_eq: "asdf"}}
                  ]
                }
                _set: {state: "asdf"}
              ) {
                affected_rows
                returning {
                  id
                  user_id
                  scenario_id
                  state
                  created
                  updated
                }
              }
            }"""
        )
        self.assertAstEqual(query, expected)

    def test_update_query__fields(self):
        root_field, query = SampleModel.update_query(
            where={"id": {"_eq": "1234"}}, set={"state": "asdf"}, fields=["id", "state"]
        )
        self.assertEquals(root_field, "update_infrastructure_deployments")
        expected = gql(
            """
            mutation infrad_update_infrastructure_deployments {
              update_infrastructure_deployments(
                where: {id: {_eq: "1234"}}
                _set: {state: "asdf"}
              ) {
                affected_rows
                returning {
                  id
                  state
                }
              }
            }"""
        )
        self.assertAstEqual(query, expected)

    def test_update_query__query_name(self):
        root_field, query = SampleModel.update_query(
            where={"id": {"_eq": "1234"}},
            set={"state": "asdf"},
            operation_name="Asdf",
        )
        self.assertEquals(root_field, "update_infrastructure_deployments")
        expected = gql(
            """
            mutation Asdf {
              update_infrastructure_deployments(
                where: {id: {_eq: "1234"}}
                _set: {state: "asdf"}
              ) {
                affected_rows
                returning {
                  id
                  user_id
                  scenario_id
                  state
                  created
                  updated
                }
              }
            }"""
        )
        self.assertAstEqual(query, expected)


class TestGqlBaseModelSubscriptionQueries(QueryTestCase):
    def test_subscription_query(self):
        root_field, query = SampleModel.subscription_query()
        self.assertEquals(root_field, "infrastructure_deployments")
        expected = gql(
            """
            subscription infrad_subscribe_infrastructure_deployments {
              infrastructure_deployments {
                id
                user_id
                scenario_id
                state
                created
                updated
              }
            }"""
        )
        self.assertAstEqual(query, expected)

    def test_subscription_query__query_name(self):
        root_field, query = SampleModel.subscription_query(operation_name="Asdf")
        self.assertEquals(root_field, "infrastructure_deployments")
        expected = gql(
            """
            subscription Asdf {
              infrastructure_deployments {
                id
                user_id
                scenario_id
                state
                created
                updated
              }
            }"""
        )
        self.assertAstEqual(query, expected)

    def test_subscription_query__fields(self):
        root_field, query = SampleModel.subscription_query(fields=["id", "state"])
        self.assertEquals(root_field, "infrastructure_deployments")
        expected = gql(
            """
            subscription infrad_subscribe_infrastructure_deployments {
              infrastructure_deployments {
                id
                state
              }
            }"""
        )
        self.assertAstEqual(query, expected)

    def test_subscription_query__where(self):
        root_field, query = SampleModel.subscription_query(where={"id": {"_eq": "asdf"}})
        self.assertEquals(root_field, "infrastructure_deployments")
        expected = gql(
            """
            subscription infrad_subscribe_infrastructure_deployments {
              infrastructure_deployments(where: {id: {_eq: "asdf"}}) {
                id
                user_id
                scenario_id
                state
                created
                updated
              }
            }"""
        )
        self.assertAstEqual(query, expected)

    def test_subscription_query__where_complex(self):
        root_field, query = SampleModel.subscription_query(
            where={
                "_and": [
                    {"id": {"_eq": "asdf"}},
                    {"state": {"_eq": "asdf"}},
                ]
            }
        )
        self.assertEquals(root_field, "infrastructure_deployments")
        expected = gql(
            """
            subscription infrad_subscribe_infrastructure_deployments {
              infrastructure_deployments(where: {_and: [{id: {_eq: "asdf"}}, {state: {_eq: "asdf"}}]}) {
                id
                user_id
                scenario_id
                state
                created
                updated
              }
            }"""
        )
        self.assertAstEqual(query, expected)


class TestGqlBaseModelStreamQueries(QueryTestCase):
    def test_stream_query(self):
        root_field, query = SampleModel.streaming_subscription_query(initial_value={"created": "2022-01-01"})
        self.assertEquals(root_field, "infrastructure_deployments_stream")
        expected = gql(
            """
            subscription infrad_stream_infrastructure_deployments {
              infrastructure_deployments_stream(
                batch_size: 1
                cursor: [{
                    initial_value: {created: "2022-01-01"}, 
                    ordering: ASC
                }]
              ){
                id
                user_id
                scenario_id
                state
                created
                updated
              }
            }"""
        )
        self.assertAstEqual(query, expected)

    def test_stream_query__batch_size(self):
        root_field, query = SampleModel.streaming_subscription_query(
            initial_value={"created": "2022-01-01"},
            batch_size=10,
        )
        self.assertEquals(root_field, "infrastructure_deployments_stream")
        expected = gql(
            """
            subscription infrad_stream_infrastructure_deployments {
              infrastructure_deployments_stream(
                batch_size: 10
                cursor: [{
                    initial_value: {created: "2022-01-01"}, 
                    ordering: ASC
                }]
              ){
                id
                user_id
                scenario_id
                state
                created
                updated
              }
            }"""
        )
        self.assertAstEqual(query, expected)

    def test_stream_query__order(self):
        root_field, query = SampleModel.streaming_subscription_query(
            initial_value={"created": "2022-01-01"},
            order="DESC",
        )
        self.assertEquals(root_field, "infrastructure_deployments_stream")
        expected = gql(
            """
            subscription infrad_stream_infrastructure_deployments {
              infrastructure_deployments_stream(
                batch_size: 1
                cursor: [{
                    initial_value: {created: "2022-01-01"}, 
                    ordering: DESC
                }]
              ){
                id
                user_id
                scenario_id
                state
                created
                updated
              }
            }"""
        )
        self.assertAstEqual(query, expected)

    def test_stream_query__query_name(self):
        root_field, query = SampleModel.streaming_subscription_query(
            initial_value={"created": "2022-01-01"}, operation_name="Asdf"
        )
        self.assertEquals(root_field, "infrastructure_deployments_stream")
        expected = gql(
            """
            subscription Asdf {
              infrastructure_deployments_stream(
                batch_size: 1
                cursor: [{
                    initial_value: {created: "2022-01-01"}, 
                    ordering: ASC
                }]
              ){
                id
                user_id
                scenario_id
                state
                created
                updated
              }
            }"""
        )
        self.assertAstEqual(query, expected)

    def test_stream_query__fields(self):
        root_field, query = SampleModel.streaming_subscription_query(
            initial_value={"created": "2022-01-01"}, fields=["id", "state"]
        )
        self.assertEquals(root_field, "infrastructure_deployments_stream")
        expected = gql(
            """
            subscription infrad_stream_infrastructure_deployments {
              infrastructure_deployments_stream(
                batch_size: 1
                cursor: [{
                    initial_value: {created: "2022-01-01"}, 
                    ordering: ASC
                }]
              ){
                id
                state
              }
            }"""
        )
        self.assertAstEqual(query, expected)

    def test_stream_query__where(self):
        root_field, query = SampleModel.streaming_subscription_query(
            initial_value={"created": "2022-01-01"}, where={"id": {"_eq": "asdf"}}
        )
        self.assertEquals(root_field, "infrastructure_deployments_stream")
        expected = gql(
            """
            subscription infrad_stream_infrastructure_deployments {
              infrastructure_deployments_stream(
                batch_size: 1
                cursor: [{
                    initial_value: {created: "2022-01-01"}, 
                    ordering: ASC
                }]
                where: {id: {_eq: "asdf"}}
              ){
                id
                user_id
                scenario_id
                state
                created
                updated
              }
            }"""
        )
        self.assertAstEqual(query, expected)

    def test_stream_query__where_complex(self):
        root_field, query = SampleModel.streaming_subscription_query(
            initial_value={"created": "2022-01-01"},
            where={
                "_and": [
                    {"id": {"_eq": "asdf"}},
                    {"state": {"_eq": "asdf"}},
                ]
            },
        )
        self.assertEquals(root_field, "infrastructure_deployments_stream")
        expected = gql(
            """
            subscription infrad_stream_infrastructure_deployments {
              infrastructure_deployments_stream(
                batch_size: 1
                cursor: [{
                    initial_value: {created: "2022-01-01"}, 
                    ordering: ASC
                }]
                where: {
                  _and: [
                    {id: {_eq: "asdf"}},
                    {state: {_eq: "asdf"}}
                  ]
                }
              ){
                id
                user_id
                scenario_id
                state
                created
                updated
              }
            }"""
        )
        self.assertAstEqual(query, expected)


class TestGqlBaseModelFetchQueries(QueryTestCase):
    def test_fetch_query(self):
        root_field, query = SampleModel.fetch_query()
        self.assertEqual(root_field, "infrastructure_deployments")
        expected = gql(
            """
            query infrad_query_infrastructure_deployments {
              infrastructure_deployments {
                id
                user_id
                scenario_id
                state
                created
                updated
              }
            }"""
        )
        self.assertAstEqual(query, expected)

    def test_fetch_query__query_name(self):
        root_field, query = SampleModel.fetch_query(operation_name="Asdf")
        self.assertEquals(root_field, "infrastructure_deployments")
        expected = gql(
            """
            query Asdf {
              infrastructure_deployments {
                id
                user_id
                scenario_id
                state
                created
                updated
              }
            }"""
        )
        self.assertAstEqual(query, expected)

    def test_fetch_query__offset(self):
        root_field, query = SampleModel.fetch_query(offset=10)
        self.assertEquals(root_field, "infrastructure_deployments")
        expected = gql(
            """
            query infrad_query_infrastructure_deployments {
              infrastructure_deployments(offset: 10) {
                id
                user_id
                scenario_id
                state
                created
                updated
              }
            }"""
        )
        self.assertAstEqual(query, expected)

    def test_fetch_query__limit(self):
        root_field, query = SampleModel.fetch_query(limit=10)
        self.assertEquals(root_field, "infrastructure_deployments")
        expected = gql(
            """
            query infrad_query_infrastructure_deployments {
              infrastructure_deployments(limit: 10) {
                id
                user_id
                scenario_id
                state
                created
                updated
              }
            }"""
        )
        self.assertAstEqual(query, expected)

    def test_fetch_query__distinct_on(self):
        root_field, query = SampleModel.fetch_query(distinct_on="id")
        self.assertEquals(root_field, "infrastructure_deployments")
        expected = gql(
            """
            query infrad_query_infrastructure_deployments {
              infrastructure_deployments(distinct_on: id) {
                id
                user_id
                scenario_id
                state
                created
                updated
              }
            }"""
        )
        self.assertAstEqual(query, expected)

    def test_fetch_query__where(self):
        root_field, query = SampleModel.fetch_query(where={"id": {"_eq": "asdf"}})
        self.assertEquals(root_field, "infrastructure_deployments")
        expected = gql(
            """
            query infrad_query_infrastructure_deployments {
              infrastructure_deployments(where: {id: {_eq: "asdf"}}) {
                id
                user_id
                scenario_id
                state
                created
                updated
              }
            }"""
        )
        self.assertAstEqual(query, expected)

    def test_fetch_query__where_complex(self):
        root_field, query = SampleModel.fetch_query(
            where={
                "_and": [
                    {"id": {"_eq": "asdf"}},
                    {"state": {"_eq": "asdf"}},
                ]
            }
        )
        self.assertEquals(root_field, "infrastructure_deployments")
        expected = gql(
            """
            query infrad_query_infrastructure_deployments {
              infrastructure_deployments(
                where: {
                  _and: [
                    {id: {_eq: "asdf"}}, 
                    {state: {_eq: "asdf"}}
                  ]
                }
              ) {
                id
                user_id
                scenario_id
                state
                created
                updated
              }
            }"""
        )
        self.assertAstEqual(query, expected)

    def test_fetch_query__fields(self):
        root_field, query = SampleModel.fetch_query(fields=["id", "state"])
        self.assertEquals(root_field, "infrastructure_deployments")
        expected = gql(
            """
            query infrad_query_infrastructure_deployments {
              infrastructure_deployments {
                id
                state
              }
            }"""
        )
        self.assertAstEqual(query, expected)

    def test_fetch_query__order_by(self):
        root_field, query = SampleModel.fetch_query(order_by=[{"created": "asc"}, {"updated": "desc"}])
        self.assertEquals(root_field, "infrastructure_deployments")
        expected = gql(
            """
            query infrad_query_infrastructure_deployments {
              infrastructure_deployments(order_by: [{created: asc}, {updated: desc}]) {
                id
                user_id
                scenario_id
                state
                created
                updated
              }
            }"""
        )
        self.assertAstEqual(query, expected)
